# Record of analysis
David R Elliott, Andrew D Thomas, Stephen R Hoon, Robin Sen (2014)

[Niche partitioning of bacterial communities in biological crusts and soils under grasses, shrubs and trees in the Kalahari](http://dx.doi.org/10.1007/s10531-014-0684-8)

Biodiversity and Conservation. April 2014

>The R Markdown (.Rmd) and R files, together with the data in this repository can be used to re-generate the entire analysis and plots associated with the above paper. 

## Compatibility
tested 16 April 2014: R 3.10, phyloseq 1.7.24, vegan 2.0-10
